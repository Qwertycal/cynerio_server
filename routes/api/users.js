const express = require('express');
const router = express.Router();
const mysql = require('mysql');

require('dotenv/config')

var con = mysql.createConnection({
  host: process.env.host ,
  user: process.env.user ,
  password: process.env.password ,
  database:  'cynerio'
});
const isConnect = false;

isConnect ? (
  con.connect(function (err) {
    if (err) throw err;
    console.log("Connected!");
  })
) : null;

router.get('/', (req, res) => {
  let sql = "SELECT * FROM `users`";
  con.query(sql, (err, result) => {
    if (err) throw err;
    return res.status(200).json({ result });
  });
});

router.post('/user', (req, res) => {
    const newUser = ({
      name: req.body.name,
      status: req.body.status
    })

    var sql = "INSERT INTO users (name) VALUES ?";
    var values = [
      [newUser.name]
    ];
    con.query(sql, [values], function (err, result) {
      if (err) throw err;
      if (result.affectedRows === 1) {
        res.status(201).send({ lastID: result.insertId })
      }
    })
  });

module.exports = router;