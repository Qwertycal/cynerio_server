const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const users = require('./routes/api/users');

const app = express();
require('dotenv/config')

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/api/users', users);

app.get('/', function (req, res) {
    res.send('I\'m alive');
})

const port = process.env.PORT || 3001;
app.listen(port, () => console.log(`server running on port : ${port}`));